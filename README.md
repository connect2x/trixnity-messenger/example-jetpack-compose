# Trixnity Messenger Demo App

A small demo app using Jetpack Compose to demonstrate on how to use
[Trixnity Messenger](https://gitlab.com/connect2x/trixnity-messenger/trixnity-messenger). It covers setup including
initializing the configuration and the dependency injection. Furthermore, the minimal dependencies that are needed to
use Trixnity Messenger are provided.

*Attention*: This is a small demo app and not a complete messenger. This would include services for notifications, and
much more. Feel free to experiment with the SDK and this app.