pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        mavenCentral()
        mavenLocal()
        google()
        maven("https://gitlab.com/api/v4/projects/47538655/packages/maven") // trixnity-messenger
    }
}

rootProject.name = "TrixnityMessengerJetpackComposeExample"
include(":app")
 