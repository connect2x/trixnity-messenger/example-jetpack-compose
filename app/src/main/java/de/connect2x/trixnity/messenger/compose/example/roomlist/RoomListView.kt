package de.connect2x.trixnity.messenger.compose.example.roomlist

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.connect2x.trixnity.messenger.compose.example.ui.theme.TrixnityMessengerJetpackComposeExampleTheme
import de.connect2x.trixnity.messenger.viewmodel.roomlist.PreviewRoomListViewModel
import de.connect2x.trixnity.messenger.viewmodel.roomlist.RoomListViewModel
import io.github.oshai.kotlinlogging.KotlinLogging

private val log = KotlinLogging.logger { }

@Composable
fun RoomListView(roomListViewModel: RoomListViewModel) {
    Column(Modifier.padding(20.dp)) {
        Text("Rooms", style = MaterialTheme.typography.headlineLarge)
        Spacer(Modifier.size(20.dp))
        RoomListElements(roomListViewModel)
    }
}

@Composable
private fun RoomListElements(roomListViewModel: RoomListViewModel) {
    val roomListElements = roomListViewModel.elements.collectAsState().value
    log.info { "roomListElements: ${roomListElements.joinToString { it.roomId.full }}" }

    LazyColumn {
        items(roomListElements, key = { it.roomId.full }) { roomListElement ->
            RoomListElementView(roomListElement)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun RoomListPreview() {
    Box(modifier = Modifier.fillMaxSize()) {
        TrixnityMessengerJetpackComposeExampleTheme {
            val roomListViewModel = PreviewRoomListViewModel()
            RoomListView(roomListViewModel)
        }
    }
}