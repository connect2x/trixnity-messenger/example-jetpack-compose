package de.connect2x.trixnity.messenger.compose.example

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.arkivanov.decompose.DefaultComponentContext
import com.arkivanov.essenty.lifecycle.LifecycleRegistry
import de.connect2x.trixnity.messenger.compose.example.ui.theme.TrixnityMessengerJetpackComposeExampleTheme
import de.connect2x.trixnity.messenger.createRoot
import de.connect2x.trixnity.messenger.multi.MatrixMultiMessenger
import de.connect2x.trixnity.messenger.multi.create
import de.connect2x.trixnity.messenger.multi.singleModeMatrixMessenger
import de.connect2x.trixnity.messenger.util.defaultActivityGetter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * This is only a small demonstration app. It does not include possibly stopping the sync when the app is put into the
 * background, etc. You can use [android.app.Service]s to implement the desired behaviour.
 */
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val coroutineScope = CoroutineScope(Dispatchers.Default)
        coroutineScope.launch {
            // create and configure a MatrixMultiMessenger as our entry-point to the SDK
            val matrixMultiMessenger =
                MatrixMultiMessenger.create(applicationContext) { messengerConfig() }
            matrixMultiMessenger.defaultActivityGetter { this@MainActivity }

            withContext(Dispatchers.Main) {// change back to UI thread
                // since we do not want the overhead of multi-tenancy, select `singleModeMatrixMessenger()` and create
                // the RootViewModel
                val matrixMessenger = matrixMultiMessenger.singleModeMatrixMessenger().first()
                val rootViewModel = matrixMessenger
                    .createRoot(componentContext = DefaultComponentContext(LifecycleRegistry()))
                setContent { // enter Compose territory
                    TrixnityMessengerJetpackComposeExampleTheme {
                        Surface(
                            modifier = Modifier.fillMaxSize(),
                            color = MaterialTheme.colorScheme.background
                        ) {
                            RootView(rootViewModel = rootViewModel)
                        }
                    }
                }
            }
        }
    }
}
