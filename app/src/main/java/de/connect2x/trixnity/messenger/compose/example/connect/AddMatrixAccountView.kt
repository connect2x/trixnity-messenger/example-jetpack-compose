package de.connect2x.trixnity.messenger.compose.example.connect

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.connect2x.trixnity.messenger.compose.example.ui.theme.TrixnityMessengerJetpackComposeExampleTheme
import de.connect2x.trixnity.messenger.viewmodel.connecting.AddMatrixAccountMethod
import de.connect2x.trixnity.messenger.viewmodel.connecting.AddMatrixAccountViewModel
import de.connect2x.trixnity.messenger.viewmodel.connecting.AddMatrixAccountViewModel.ServerDiscoveryState
import de.connect2x.trixnity.messenger.viewmodel.connecting.PreviewAddMatrixAccountViewModel
import net.folivo.trixnity.clientserverapi.model.authentication.LoginType

@Composable
fun AddMatrixAccountView(addMatrixAccountViewModel: AddMatrixAccountViewModel) {
    val serverUrl = addMatrixAccountViewModel.serverUrl.collectAsState().value
    val serverDiscoveryState = addMatrixAccountViewModel.serverDiscoveryState.collectAsState().value

    Column(Modifier.padding(20.dp)) {
        Text("Chose a Matrix Server", style = MaterialTheme.typography.headlineLarge)
        Column(Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center) {
            OutlinedTextField(
                value = serverUrl.text,
                onValueChange = { value -> addMatrixAccountViewModel.serverUrl.update(value) },
                label = { Text("Matrix-Server") },
                modifier = Modifier.fillMaxWidth(),
            )
            Spacer(Modifier.size(20.dp))
            Box(
                Modifier.fillMaxWidth(),
                contentAlignment = Alignment.Center
            ) {
                when (serverDiscoveryState) {
                    is ServerDiscoveryState.None -> Box {}
                    is ServerDiscoveryState.Loading -> CircularProgressIndicator()
                    is ServerDiscoveryState.Failure -> Text("Cannot determine the Matrix server: ${serverDiscoveryState.message}")
                    is ServerDiscoveryState.Success -> ServerDiscoverySuccessView(
                        addMatrixAccountViewModel,
                        serverDiscoveryState
                    )
                }
            }
        }
    }
}

@Composable
private fun ServerDiscoverySuccessView(
    addMatrixAccountViewModel: AddMatrixAccountViewModel,
    serverDiscoverySuccess: ServerDiscoveryState.Success
) {
    Column(
        Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        serverDiscoverySuccess.addMatrixAccountMethods.mapIndexed { index, addMatrixAccountMethod ->
            if (index > 0) {
                Spacer(Modifier.size(20.dp))
            }
            when (addMatrixAccountMethod) {
                is AddMatrixAccountMethod.Password -> Button(
                    onClick = { addMatrixAccountViewModel.selectAddMatrixAccountMethod(addMatrixAccountMethod) }) {
                    Text(text = "Login with Password")
                }

                is AddMatrixAccountMethod.SSO -> Text(text = "Login with ${addMatrixAccountMethod.identityProvider?.name}")
                is AddMatrixAccountMethod.Register -> Text(text = "Register as a new User")
            }
        }
    }
}

@SuppressLint("StateFlowValueCalledInComposition")
@Preview(showBackground = true)
@Composable
fun AddMatrixAccountPreview() {
    TrixnityMessengerJetpackComposeExampleTheme {
        Box(modifier = Modifier.fillMaxSize()) {
            val addMatrixAccountViewModel = PreviewAddMatrixAccountViewModel()
//            addMatrixAccountViewModel.serverDiscoveryState.value = ServerDiscoveryState.Loading
//            addMatrixAccountViewModel.serverDiscoveryState.value = ServerDiscoveryState.Failure("Oh no!")
            addMatrixAccountViewModel.serverDiscoveryState.value = ServerDiscoveryState.Success(
                setOf(
                    AddMatrixAccountMethod.Password("matrix.org"),
                    AddMatrixAccountMethod.SSO(
                        "matrix.org",
                        LoginType.SSO.IdentityProvider(id = "google", name = "google"),
                        icon = null,
                    ),
                    AddMatrixAccountMethod.Register("matrix.org"),
                )
            )

            AddMatrixAccountView(addMatrixAccountViewModel = addMatrixAccountViewModel)
        }
    }
}