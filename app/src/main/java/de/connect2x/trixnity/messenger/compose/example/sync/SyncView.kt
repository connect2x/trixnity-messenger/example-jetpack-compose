package de.connect2x.trixnity.messenger.compose.example.sync

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.connect2x.trixnity.messenger.compose.example.ui.theme.TrixnityMessengerJetpackComposeExampleTheme
import de.connect2x.trixnity.messenger.viewmodel.initialsync.AccountSync
import de.connect2x.trixnity.messenger.viewmodel.initialsync.PreviewSyncViewModel
import de.connect2x.trixnity.messenger.viewmodel.initialsync.SyncViewModel

@Composable
fun SyncView(syncViewModel: SyncViewModel) {
    val accountSyncStates = syncViewModel.accountSyncStates.collectAsState().value
    Surface(modifier = Modifier.fillMaxSize(), color = Color.Black.copy(alpha = 0.4f)) {
        Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Surface(modifier = Modifier.sizeIn(minWidth = 200.dp, minHeight = 150.dp)) {
                Column(Modifier.padding(20.dp)) {
                    accountSyncStates?.map { (accountName, accountSync) ->
                        val sync = when (accountSync) {
                            AccountSync.DONE -> "Done"
                            AccountSync.INITIAL_SYNC -> "Initial Sync"
                        }
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Text("$accountName: $sync", Modifier.padding(vertical = 10.dp))
                            Spacer(Modifier.size(10.dp))
                            if (accountSync == AccountSync.DONE) {
                                Icon(Icons.Default.Done, "Done")
                            } else {
                                CircularProgressIndicator(Modifier.size(24.dp))
                            }
                        }
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SyncPreview() {
    Box(modifier = Modifier.fillMaxSize()) {
        TrixnityMessengerJetpackComposeExampleTheme {
            val syncViewModel = PreviewSyncViewModel()
            SyncView(syncViewModel)
        }
    }
}