package de.connect2x.trixnity.messenger.compose.example

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.arkivanov.decompose.extensions.compose.stack.Children
import com.arkivanov.decompose.extensions.compose.stack.animation.fade
import com.arkivanov.decompose.extensions.compose.stack.animation.stackAnimation
import com.arkivanov.decompose.router.stack.ChildStack
import com.arkivanov.decompose.value.Value
import de.connect2x.trixnity.messenger.compose.example.roomlist.RoomListView
import de.connect2x.trixnity.messenger.compose.example.sync.SyncView
import de.connect2x.trixnity.messenger.viewmodel.MainViewModel
import de.connect2x.trixnity.messenger.viewmodel.initialsync.InitialSyncRouter
import de.connect2x.trixnity.messenger.viewmodel.roomlist.RoomListRouter

@Composable
fun MainView(mainViewModel: MainViewModel) {
    Box(Modifier.fillMaxSize()) {
        RoomListSwitch(mainViewModel.roomListRouterStack)
        // the initial sync should be displayed on top of everything else
        InitialSyncSwitch(mainViewModel.initialSyncStack)
    }
}

@Composable
private fun InitialSyncSwitch(initialSyncStack: Value<ChildStack<InitialSyncRouter.Config, InitialSyncRouter.Wrapper>>) {
    Children(stack = initialSyncStack) {
        when (val child = it.instance) {
            is InitialSyncRouter.Wrapper.None -> Box {}
            is InitialSyncRouter.Wrapper.Undefined -> Text("Initial Sync is in an undefined state")
            is InitialSyncRouter.Wrapper.Sync -> SyncView(child.viewModel)
        }
    }
}

@Composable
private fun RoomListSwitch(roomListRouterStack: Value<ChildStack<RoomListRouter.Config, RoomListRouter.Wrapper>>) {
    Children(stack = roomListRouterStack, animation = stackAnimation(fade())) {
        when (val child = it.instance) {
            is RoomListRouter.Wrapper.None -> Box {}
            is RoomListRouter.Wrapper.List -> RoomListView(child.viewModel)
            else -> Text("State of RoomListRouter: ${child::class::qualifiedName}")
        }
    }
}
