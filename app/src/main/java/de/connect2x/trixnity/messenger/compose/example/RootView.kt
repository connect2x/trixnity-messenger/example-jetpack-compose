package de.connect2x.trixnity.messenger.compose.example

import androidx.compose.foundation.layout.Box
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.arkivanov.decompose.extensions.compose.stack.Children
import com.arkivanov.decompose.extensions.compose.stack.animation.fade
import com.arkivanov.decompose.extensions.compose.stack.animation.stackAnimation
import com.arkivanov.decompose.router.stack.ChildStack
import com.arkivanov.decompose.value.Value
import de.connect2x.trixnity.messenger.compose.example.connect.AddMatrixAccountView
import de.connect2x.trixnity.messenger.compose.example.connect.MatrixClientInitializationView
import de.connect2x.trixnity.messenger.compose.example.connect.PasswordLoginView
import de.connect2x.trixnity.messenger.viewmodel.RootRouter
import de.connect2x.trixnity.messenger.viewmodel.RootViewModel

@Composable
fun RootView(rootViewModel: RootViewModel) {
    RootSwitch(rootViewModel.stack)
}

@Composable
private fun RootSwitch(rootStack: Value<ChildStack<RootRouter.Config, RootRouter.Wrapper>>) {
    // Children is a helper from `com.arkivanov.decompose:extensions-compose-jetbrains`,
    // see https://arkivanov.github.io/Decompose/extensions/compose/#navigating-between-composable-components
    Children(stack = rootStack, animation = stackAnimation(fade())) {
        when(val child = it.instance) {
            is RootRouter.Wrapper.None -> Box {}
            is RootRouter.Wrapper.Main -> MainView(child.viewModel)
            is RootRouter.Wrapper.MatrixClientInitialization -> MatrixClientInitializationView(child.viewModel)
            is RootRouter.Wrapper.AddMatrixAccount -> AddMatrixAccountView(child.viewModel)
            is RootRouter.Wrapper.PasswordLogin -> PasswordLoginView(child.viewModel)
            else -> ImplementMeView(child)
        }
    }
}

@Composable
private fun ImplementMeView(child: RootRouter.Wrapper) {
    Text("The state ${child::class.qualifiedName} needs to be implemented.")
}