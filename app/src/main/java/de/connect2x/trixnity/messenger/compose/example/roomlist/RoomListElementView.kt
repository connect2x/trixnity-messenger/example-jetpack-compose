package de.connect2x.trixnity.messenger.compose.example.roomlist

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Divider
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import de.connect2x.trixnity.messenger.viewmodel.roomlist.RoomListElementViewModel

@Composable
fun RoomListElementView(roomListElementViewModel: RoomListElementViewModel) {
    val name = roomListElementViewModel.roomName.collectAsState().value ?: roomListElementViewModel.roomId.full
    val isDirect = roomListElementViewModel.isDirect.collectAsState().value

    Column(Modifier.padding(bottom = 10.dp)) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            if (isDirect != null) {
                if (isDirect) {
                    Icon(Icons.Default.Person, "direct")
                } else {
                    Box(contentAlignment = Alignment.Center) {
                        Icon(Icons.Default.Person, "group")
                        Icon(Icons.Default.Person, "group", Modifier.padding(top = 3.dp, start = 3.dp), Color.White)
                        Icon(Icons.Default.Person, "group", Modifier.padding(top = 6.dp, start = 6.dp))
                    }
                }
            }
            Text(text = name, style = MaterialTheme.typography.bodyMedium)
        }
        Spacer(Modifier.size(10.dp))
        HorizontalDivider(Modifier.fillMaxWidth())
    }
}

